# 日本 Preparation

For this demo, git webhooks (gitlab, github, etc) are "emulated"

## Scalable web app

CI/CD pipeline for scalable web application from source code to deployment on top
of Kubernetes

### Architecture overview

#### Infrastructure

CI => build docker images
CD => deploy docker images (blue-green deployment)
git => dev*, test, acceptance?, production branches
k3(8)s => minikube
jenkins

what does jenkins do?

build docker images

what does kubernetes do?

scale apps
namespaces:
    test
    acceptance?
    production

#### Application

QoS

### Implemented components

### Benefit of the concept

### Operational consideration (availability, monitoring, lifecycle, etc.)

kubernetes services
prometheus
grafana

## Data analytic pipeline from streaming data to visualization

what does streaming means here?
maybe something like twitter stream?
    read data with no downtime

ELK

## Machine Learning as a Service

Still have no clue
