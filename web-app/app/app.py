import argparse
import platform
import sys

from flask import Flask
import yaml


app = Flask(__name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--configfile', action='store')
    return parser.parse_args()


def load_configuration(path=None):
    try:
        with open(path, "r") as fd:
            return yaml.safe_load(fd)
    except yaml.YAMLError as yml_error:
        print(yml_error)
        sys.exit(2)


@app.route("/")
def main():
    return platform.node()


if __name__ == "__main__":
    args = parse_args()
    if not args.configfile:
        print("--configfile is required")
        sys.exit(2)

    config = load_configuration(path=args.configfile)

    app.run(host=config["app_host"], port=config["app_port"])
