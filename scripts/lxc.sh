#/bin/bash

lxc launch ubuntu:19.10 openstack-controller-0
lxc launch ubuntu:19.10 openstack-compute-0
lxc launch ubuntu:19.10 ceph-0
lxc launch ubuntu:19.10 jenkins-master-0
lxc launch ubuntu:19.10 jenkins-worker-0
lxc launch ubuntu:19.10 monitoring-0
lxc launch ubuntu:19.10 k3s-master-0
lxc launch ubuntu:19.10 k3s-worker-0

cat ~/.ssh/id_rsa.pub | lxc file edit openstack-controller-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit openstack-compute-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit jenkins-master-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit jenkins-worker-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit ceph-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit monitoring-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit k3s-master-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | lxc file edit k3s-worker-0/root/.ssh/authorized_keys